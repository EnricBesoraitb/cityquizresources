package cat.itb.cityquiz.data.citiyfiledata;

public class CityJson {
    public static final String json = "[{\"name\": \"Hong Kong\",\"country\": \"Hong Kong\"},\n" +
            "{\"name\": \"Singapore\",\"country\": \"Singapore\"},\n" +
            "{\"name\": \"Bangkok\",\"country\": \"Thailand\"},\n" +
            "{\"name\": \"London\",\"country\": \"UK\"},\n" +
            "{\"name\": \"Macau\",\"country\": \"Macau\"},\n" +
            "{\"name\": \"Kuala Lumpur\",\"country\": \"Malaysia\"},\n" +
            "{\"name\": \"Shenzhen\",\"country\": \"China\"},\n" +
            "{\"name\": \"New York City\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Antalya\",\"country\": \"Turkey\"},\n" +
            "{\"name\": \"Paris\",\"country\": \"France\"},\n" +
            "{\"name\": \"Istanbul\",\"country\": \"Turkey\"},\n" +
            "{\"name\": \"Rome\",\"country\": \"Italy\"},\n" +
            "{\"name\": \"Dubai\",\"country\": \"UAE\"},\n" +
            "{\"name\": \"Guangzhou\",\"country\": \"China\"},\n" +
            "{\"name\": \"Phuket\",\"country\": \"Thailand\"},\n" +
            "{\"name\": \"Mecca\",\"country\": \"Saudi Arabia\"},\n" +
            "{\"name\": \"Pattaya\",\"country\": \"Thailand\"},\n" +
            "{\"name\": \"Taipei City\",\"country\": \"Taiwan\"},\n" +
            "{\"name\": \"Prague\",\"country\": \"Czech Republic\"},\n" +
            "{\"name\": \"Shanghai\",\"country\": \"China\"},\n" +
            "{\"name\": \"Las Vegas\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Miami\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Barcelona\",\"country\": \"Spain\"},\n" +
            "{\"name\": \"Moscow\",\"country\": \"Russia\"},\n" +
            "{\"name\": \"Beijing\",\"country\": \"China\"},\n" +
            "{\"name\": \"Los Angeles\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Budapest\",\"country\": \"Hungary\"},\n" +
            "{\"name\": \"Vienna\",\"country\": \"Austria\"},\n" +
            "{\"name\": \"Amsterdam\",\"country\": \"Netherlands\"},\n" +
            "{\"name\": \"Sofia\",\"country\": \"Bulgaria\"},\n" +
            "{\"name\": \"Madrid\",\"country\": \"Spain\"},\n" +
            "{\"name\": \"Orlando\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Ho Chi Minh City\",\"country\": \"Vietnam\"},\n" +
            "{\"name\": \"Lima\",\"country\": \"Peru\"},\n" +
            "{\"name\": \"Berlin\",\"country\": \"Germany\"},\n" +
            "{\"name\": \"Tokyo\",\"country\": \"Japan\"},\n" +
            "{\"name\": \"Warsaw\",\"country\": \"Poland\"},\n" +
            "{\"name\": \"Chennai\",\"country\": \"India\"},\n" +
            "{\"name\": \"Cairo\",\"country\": \"Egypt\"},\n" +
            "{\"name\": \"Nairobi\",\"country\": \"Kenya\"},\n" +
            "{\"name\": \"Hangzhou\",\"country\": \"China\"},\n" +
            "{\"name\": \"Milan\",\"country\": \"Italy\"},\n" +
            "{\"name\": \"San Francisco\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Buenos Aires\",\"country\": \"Argentina\"},\n" +
            "{\"name\": \"Venice\",\"country\": \"Italy\"},\n" +
            "{\"name\": \"Mexico City\",\"country\": \"Mexico\"},\n" +
            "{\"name\": \"Dublin\",\"country\": \"Ireland\"},\n" +
            "{\"name\": \"Seoul\",\"country\": \"South Korea\"},\n" +
            "{\"name\": \"Mugla\",\"country\": \"Turkey\"},\n" +
            "{\"name\": \"Mumbai\",\"country\": \"India\"},\n" +
            "{\"name\": \"Denpasar\",\"country\": \"Indonesia\"},\n" +
            "{\"name\": \"Delhi\",\"country\": \"India\"},\n" +
            "{\"name\": \"Toronto\",\"country\": \"Canada\"},\n" +
            "{\"name\": \"Zhuhai\",\"country\": \"China\"},\n" +
            "{\"name\": \"St Petersburg\",\"country\": \"Russia\"},\n" +
            "{\"name\": \"Burgas\",\"country\": \"Bulgaria\"},\n" +
            "{\"name\": \"Sydney\",\"country\": \"Australia\"},\n" +
            "{\"name\": \"Djerba\",\"country\": \"Tunisia\"},\n" +
            "{\"name\": \"Munich\",\"country\": \"Germany\"},\n" +
            "{\"name\": \"Johannesburg\",\"country\": \"South Africa\"},\n" +
            "{\"name\": \"Cancun\",\"country\": \"Mexico\"},\n" +
            "{\"name\": \"Edirne\",\"country\": \"Turkey\"},\n" +
            "{\"name\": \"Suzhou\",\"country\": \"China\"},\n" +
            "{\"name\": \"Bucharest\",\"country\": \"Romania\"},\n" +
            "{\"name\": \"Punta Cana\",\"country\": \"Dominican Republic\"},\n" +
            "{\"name\": \"Agra\",\"country\": \"India\"},\n" +
            "{\"name\": \"Jaipur\",\"country\": \"India\"},\n" +
            "{\"name\": \"Brussels\",\"country\": \"Belgium\"},\n" +
            "{\"name\": \"Nice\",\"country\": \"France\"},\n" +
            "{\"name\": \"Chiang Mai\",\"country\": \"Thailand\"},\n" +
            "{\"name\": \"Sharm el-Sheikh\",\"country\": \"Egypt\"},\n" +
            "{\"name\": \"Lisbon\",\"country\": \"Portugal\"},\n" +
            "{\"name\": \"East Province\",\"country\": \"Saudi Arabia\"},\n" +
            "{\"name\": \"Marrakech\",\"country\": \"Morocco\"},\n" +
            "{\"name\": \"Jakarta\",\"country\": \"Indonesia\"},\n" +
            "{\"name\": \"Manama\",\"country\": \"Bahrain\"},\n" +
            "{\"name\": \"Hanoi\",\"country\": \"Vietnam\"},\n" +
            "{\"name\": \"Honolulu\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Manila\",\"country\": \"Philippines\"},\n" +
            "{\"name\": \"Guilin\",\"country\": \"China\"},\n" +
            "{\"name\": \"Auckland\",\"country\": \"New Zealand\"},\n" +
            "{\"name\": \"Siem Reap\",\"country\": \"Cambodia\"},\n" +
            "{\"name\": \"Sousse\",\"country\": \"Tunisia\"},\n" +
            "{\"name\": \"Amman\",\"country\": \"Jordan\"},\n" +
            "{\"name\": \"Vancouver\",\"country\": \"Canada\"},\n" +
            "{\"name\": \"Abu Dhabi\",\"country\": \"UAE\"},\n" +
            "{\"name\": \"Kiev\",\"country\": \"Ukraine\"},\n" +
            "{\"name\": \"Doha\",\"country\": \"Qatar\"},\n" +
            "{\"name\": \"Florence\",\"country\": \"Italy\"},\n" +
            "{\"name\": \"Rio de Janeiro\",\"country\": \"Brazil\"},\n" +
            "{\"name\": \"Melbourne\",\"country\": \"Australia\"},\n" +
            "{\"name\": \"Washington D.C.\",\"country\": \"USA\"},\n" +
            "{\"name\": \"Riyadh\",\"country\": \"Saudi Arabia\"},\n" +
            "{\"name\": \"Christchurch\",\"country\": \"New Zealand\"},\n" +
            "{\"name\": \"Frankfurt\",\"country\": \"Germany\"},\n" +
            "{\"name\": \"Baku\",\"country\": \"Azerbaijan\"},\n" +
            "{\"name\": \"Sao Paulo\",\"country\": \"Brazil\"},\n" +
            "{\"name\": \"Harare\",\"country\": \"Zimbabwe\"},\n" +
            "{\"name\": \"Kolkata\",\"country\": \"India\"},\n" +
            "{\"name\": \"Nanjing\",\"country\": \"China\"}]";

    /*
    [{"name": "Hong Kong","country": "Hong Kong"},
{"name": "Singapore","country": "Singapore"},
{"name": "Bangkok","country": "Thailand"},
{"name": "London","country": "UK"},
{"name": "Macau","country": "Macau"},
{"name": "Kuala Lumpur","country": "Malaysia"},
{"name": "Shenzhen","country": "China"},
{"name": "New York City","country": "USA"},
{"name": "Antalya","country": "Turkey"},
{"name": "Paris","country": "France"},
{"name": "Istanbul","country": "Turkey"},
{"name": "Rome","country": "Italy"},
{"name": "Dubai","country": "UAE"},
{"name": "Guangzhou","country": "China"},
{"name": "Phuket","country": "Thailand"},
{"name": "Mecca","country": "Saudi Arabia"},
{"name": "Pattaya","country": "Thailand"},
{"name": "Taipei City","country": "Taiwan"},
{"name": "Prague","country": "Czech Republic"},
{"name": "Shanghai","country": "China"},
{"name": "Las Vegas","country": "USA"},
{"name": "Miami","country": "USA"},
{"name": "Barcelona","country": "Spain"},
{"name": "Moscow","country": "Russia"},
{"name": "Beijing","country": "China"},
{"name": "Los Angeles","country": "USA"},
{"name": "Budapest","country": "Hungary"},
{"name": "Vienna","country": "Austria"},
{"name": "Amsterdam","country": "Netherlands"},
{"name": "Sofia","country": "Bulgaria"},
{"name": "Madrid","country": "Spain"},
{"name": "Orlando","country": "USA"},
{"name": "Ho Chi Minh City","country": "Vietnam"},
{"name": "Lima","country": "Peru"},
{"name": "Berlin","country": "Germany"},
{"name": "Tokyo","country": "Japan"},
{"name": "Warsaw","country": "Poland"},
{"name": "Chennai","country": "India"},
{"name": "Cairo","country": "Egypt"},
{"name": "Nairobi","country": "Kenya"},
{"name": "Hangzhou","country": "China"},
{"name": "Milan","country": "Italy"},
{"name": "San Francisco","country": "USA"},
{"name": "Buenos Aires","country": "Argentina"},
{"name": "Venice","country": "Italy"},
{"name": "Mexico City","country": "Mexico"},
{"name": "Dublin","country": "Ireland"},
{"name": "Seoul","country": "South Korea"},
{"name": "Mugla","country": "Turkey"},
{"name": "Mumbai","country": "India"},
{"name": "Denpasar","country": "Indonesia"},
{"name": "Delhi","country": "India"},
{"name": "Toronto","country": "Canada"},
{"name": "Zhuhai","country": "China"},
{"name": "St Petersburg","country": "Russia"},
{"name": "Burgas","country": "Bulgaria"},
{"name": "Sydney","country": "Australia"},
{"name": "Djerba","country": "Tunisia"},
{"name": "Munich","country": "Germany"},
{"name": "Johannesburg","country": "South Africa"},
{"name": "Cancun","country": "Mexico"},
{"name": "Edirne","country": "Turkey"},
{"name": "Suzhou","country": "China"},
{"name": "Bucharest","country": "Romania"},
{"name": "Punta Cana","country": "Dominican Republic"},
{"name": "Agra","country": "India"},
{"name": "Jaipur","country": "India"},
{"name": "Brussels","country": "Belgium"},
{"name": "Nice","country": "France"},
{"name": "Chiang Mai","country": "Thailand"},
{"name": "Sharm el-Sheikh","country": "Egypt"},
{"name": "Lisbon","country": "Portugal"},
{"name": "East Province","country": "Saudi Arabia"},
{"name": "Marrakech","country": "Morocco"},
{"name": "Jakarta","country": "Indonesia"},
{"name": "Manama","country": "Bahrain"},
{"name": "Hanoi","country": "Vietnam"},
{"name": "Honolulu","country": "USA"},
{"name": "Manila","country": "Philippines"},
{"name": "Guilin","country": "China"},
{"name": "Auckland","country": "New Zealand"},
{"name": "Siem Reap","country": "Cambodia"},
{"name": "Sousse","country": "Tunisia"},
{"name": "Amman","country": "Jordan"},
{"name": "Vancouver","country": "Canada"},
{"name": "Abu Dhabi","country": "UAE"},
{"name": "Kiev","country": "Ukraine"},
{"name": "Doha","country": "Qatar"},
{"name": "Florence","country": "Italy"},
{"name": "Rio de Janeiro","country": "Brazil"},
{"name": "Melbourne","country": "Australia"},
{"name": "Washington D.C.","country": "USA"},
{"name": "Riyadh","country": "Saudi Arabia"},
{"name": "Christchurch","country": "New Zealand"},
{"name": "Frankfurt","country": "Germany"},
{"name": "Baku","country": "Azerbaijan"},
{"name": "Sao Paulo","country": "Brazil"},
{"name": "Harare","country": "Zimbabwe"},
{"name": "Kolkata","country": "India"},
{"name": "Nanjing","country": "China"}]
     */
}
