package cat.itb.cityquiz.presentation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class CityQuizViewModel extends ViewModel {
    private GameLogic gameLogic = RepositoriesFactory.getGameLogic();
    private MutableLiveData<Game> gameLiveData = new MutableLiveData<>();

    public void startQuiz(){
        Game game = gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers);
        gameLiveData.postValue(game);
    }

    public LiveData<Game> getGame(){
        return this.gameLiveData;
    }

    public void answerQuestion(int resposta){

        Game game = gameLogic.answerQuestions(gameLiveData.getValue(),resposta);
        gameLiveData.postValue(game);
    }


}
