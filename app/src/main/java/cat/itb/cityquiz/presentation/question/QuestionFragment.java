package cat.itb.cityquiz.presentation.question;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.List;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.domain.Question;
import cat.itb.cityquiz.presentation.CityQuizViewModel;

public class QuestionFragment extends Fragment {

    private CityQuizViewModel mViewModel;
    private Button city1;
    private Button city2;
    private Button city3;
    private Button city4;
    private Button city5;
    private Button city6;
    private ImageView imageView;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);

    }

    private void onGameChanged(Game game) {
        if (game.isFinished()){
            navigateToGameResult();
        } else {
            display(game);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.city1).setOnClickListener(this::changeFragment);
        imageView = getView().findViewById(R.id.cityImage);

        //Set buttons
        city1 = view.findViewById(R.id.city1);
        city1.setTag(0);
        city1.setOnClickListener(this::changeFragment);


        city2 = view.findViewById(R.id.city2);
        city2.setTag(1);
        city2.setOnClickListener(this::changeFragment);


        city3 = view.findViewById(R.id.city3);
        city3.setTag(2);
        city3.setOnClickListener(this::changeFragment);


        city4 = view.findViewById(R.id.city4);
        city4.setTag(3);
        city4.setOnClickListener(this::changeFragment);


        city5 = view.findViewById(R.id.city5);
        city5.setTag(4);
        city5.setOnClickListener(this::changeFragment);


        city6 = view.findViewById(R.id.city6);
        city6.setTag(5);
        city6.setOnClickListener(this::changeFragment);
    }

    private void display(Game game) {
        Question currentQuestion = game.getCurrentQuestion();
        List<City> cityList = currentQuestion.getPossibleCities();
        city1.setText(cityList.get(0).getName());
        city2.setText(cityList.get(1).getName());
        city3.setText(cityList.get(2).getName());
        city4.setText(cityList.get(3).getName());
        city5.setText(cityList.get(4).getName());
        city6.setText(cityList.get(5).getName());


        String fileName = ImagesDownloader.scapeName(game.getCurrentQuestion().getCorrectCity().getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageView.setImageResource(resId);
    }

    private void changeFragment(View view) {
        int number = (Integer) view.getTag();
        mViewModel.answerQuestion(number);
        Game game = mViewModel.getGame().getValue();
    }

    private void navigateToGameResult() {
        Navigation.findNavController(getView()).navigate(R.id.endGame);
    }

}
