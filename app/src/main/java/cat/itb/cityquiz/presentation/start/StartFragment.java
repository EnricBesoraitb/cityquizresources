package cat.itb.cityquiz.presentation.start;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import cat.itb.cityquiz.R;
import cat.itb.cityquiz.presentation.CityQuizViewModel;

public class StartFragment extends Fragment {

    private CityQuizViewModel mViewModel;


    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.start_fragment, container, false);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(CityQuizViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.startButton).setOnClickListener(this::changeFragment);
    }

    private void changeFragment(View view) {
        mViewModel.startQuiz();
        navigateToQuiz();
    }

    private void navigateToQuiz() {
        Navigation.findNavController(getView()).navigate(R.id.beginGame);
    }
}
