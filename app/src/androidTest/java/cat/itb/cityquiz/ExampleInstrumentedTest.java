package cat.itb.cityquiz;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cat.itb.cityquiz.presentation.MainActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);
    @Test
    public void useAppContext() {

        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        onView(withId(R.id.startButton)).perform(click());
        onView(withId(R.id.cityImage)).check(matches(isDisplayed()));
        onView(withId(R.id.city1)).perform(click());
        onView(withId(R.id.cityImage)).check(matches(isDisplayed()));
        onView(withId(R.id.city2)).perform(click());
        onView(withId(R.id.cityImage)).check(matches(isDisplayed()));
        onView(withId(R.id.city3)).perform(click());
        onView(withId(R.id.cityImage)).check(matches(isDisplayed()));
        onView(withId(R.id.city4)).perform(click());
        onView(withId(R.id.cityImage)).check(matches(isDisplayed()));
        onView(withId(R.id.city6)).perform(click());
        onView(withId(R.id.scoreView)).check(matches(isDisplayed()));

    }
}
